<?php

namespace Hosterra\NetBox\Api\DCIM;

use GuzzleHttp\Exception\GuzzleException;
use Hosterra\NetBox\Api\AbstractApi;

class ConnectedDevices extends AbstractApi {
	/**
	 * @param array $params
	 *
	 * @return mixed
	 * @throws GuzzleException
	 */
	public function list( array $params = [] ) {
		return $this->get( "/dcim/connected-device/", $params );
	}
}
